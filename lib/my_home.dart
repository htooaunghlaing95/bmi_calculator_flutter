import 'package:flutter/material.dart';

class MyHome extends StatefulWidget {
  @override
  _MyHomeState createState() => _MyHomeState();
}

class _MyHomeState extends State<MyHome> {

  var tController1 = TextEditingController();
  var tController2 = TextEditingController();
  var tController3 = TextEditingController();

  var result = "Result";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.lightGreen,
        title: titleWidget(),
      ),

      body: Container(
        padding: EdgeInsets.only(left: 20.0, right: 20.0),
        child: Column(
          children: <Widget>[
            SizedBox(height: 20.0),

            firstTF(),

            SizedBox(height: 20.0),

            secondTF(),

            SizedBox(height: 20.0),

            thirdTF(),

            SizedBox(height: 20.0),

            buttonBarWidget(),

            SizedBox(height: 20.0),

            Text("$result", style: TextStyle(
              color: Colors.green,
              fontSize: 30.0,
            ),
            ),
          ],
        ),
      ),
    );
  }

  Widget firstTF(){
    return TextField(
      keyboardType: TextInputType.number,
      controller: tController1,
      decoration: InputDecoration(
        labelText: "Enter Weight",
        hintText: "Enter Weight in lbs",
        border: OutlineInputBorder(

        ),
      ),
    );
  }

  Widget secondTF(){
    return TextField(
      keyboardType: TextInputType.number,
      controller: tController2,
      decoration: InputDecoration(
        labelText: "Enter Height(Feet)",
        hintText: "Enter Number only",
        border: OutlineInputBorder(

        ),
      ),

    );
  }

  Widget thirdTF(){
    return TextField(
      keyboardType: TextInputType.number,
      controller: tController3,
      decoration: InputDecoration(
        labelText: "Enter Height(Inches)",
        hintText: "Enter Number only",
        border: OutlineInputBorder(

        ),
      ),

    );
  }

  buttonBarWidget(){

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,

      children: <Widget>[
        RaisedButton(
          color: Colors.green,
          textColor: Colors.white,
          child: Text("Calculate BMI", style: TextStyle(
            fontSize: 20.0,
          ),
          ),
          onPressed: () {
            _calculateBMI();
          },
        ),
      ],
    );

  }

  _calculateBMI() {

    double weight = double.parse(tController1.text);
    double feet = double.parse(tController2.text);
    double inches = double.parse(tController3.text);

    double totalInches = (feet*12)+inches;

    var bmi = (weight*703)/(totalInches*totalInches);


    if(bmi < 18.5) {
      result = "You are underweight";
    } else if (bmi >= 18.5 || bmi <= 24.9) {
      result = "You are Normal Weight";
    } else if (bmi >= 25 || bmi <= 29.9){
      result = "You are Overweight";
    } else {
      result = "You are Obesity";
    }

  setState(() {

  });
  }

  Widget titleWidget() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text("BMI Calculator")
      ],
    );
  }

}